global data_log;

data_log = struct('xk',[],'uk',[],'vk',[],'tk',[],'rk',[],'rjk',[],'xrjk',[],'cstate',[],'xc',[],...
    'ek',[],'G',[]);

data_log.xk = x0;
data_log.ek = 0;
data_log.uk = (abar-a)*x0;
data_log.vk = 0;
data_log.tk = 1;
data_log.rk = 1;
data_log.rjk = 0;
data_log. xrjk = x0;
data_log.cstate = state0;
data_log.xc = x0;
data_log.G = 0;
data_log.J = 0;