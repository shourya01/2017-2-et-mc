global a;
global abar;
global M;
global c;
global n;
global P0;
global P1;
global e;
global B;
global x0;
global D;
global state0;
global max_iter;
global plot_enable;

% Define the above;

a = 1.15;
abar = 0.78*0.95;
M = 1;
c= 0.95;
n = 4;
% -------------------
% P0 = gen_stoch_matrix(n);
% P1 = gen_stoch_matrix(n);
% -------------------
P1 = [0.679,0.179,0.129,0.013;0.052,0.925,0.023,0;...
    0.104,0.007,0.75,0.139;0,0,0.778,0.222];
P0 = P1;
e = [0.2244,0.3128,0.1031,0.5400];
% -------------------
% P0 = (1/n)*ones(n,n);
% P1 = P0;
% e = 0.3*ones(1,n);
% -------------------
% P0 = gen_stoch_matrix(n,5);
% P1 = gen_stoch_matrix(n,6);
% e = [0.1343, 0.1930, 0.2, 0.2560];
B = 12;
x0 = 20*B;
state0 = 3;
max_iter = 400;
% D = 1;
plot_enable = 1;