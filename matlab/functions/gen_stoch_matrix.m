function T = gen_stoch_matrix(n,varargin)

% This function will generate a n times n
% right stochastic matrix

if nargin > 1
    rng(varargin{1});
else
    rng('shuffle');
end

T = rand(n,n);

for i = 1:n
    T(:,i) = T(:,i)/sum(T(:,i));
end

end


