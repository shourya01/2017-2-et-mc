function T = gen_stoch_vector(n,varargin)

% This function will generate a column
% stochastic vector of n elements

if nargin > 1
    seedval = varargin{1};
    rng(seedval);
else
    rng('shuffle');
end

T = rand(n,1);
T = T/sum(T);

end


