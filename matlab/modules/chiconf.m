function [C] = chiconf(b,D,P0,P1,e,state_chk)

% Code

n = size(P1,1);

dist = zeros(n,1);
dist(state_chk) = 1;
C = (P0^(D))*dist;
C = inv(eye(n) - b*P1*diag(e))*C;
C = diag(ones(1,n)-e)*C;
C = ones(1,n)*C;
C = (b.^D)*C;

end