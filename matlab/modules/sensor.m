function [ek,tk,G,J] = sensor(iter,rjk,xrjk,B,D,tk_old,state_old,xk,abar,a,c,M,P0,P1,e,xc_old)

% Code

ek = xk - (abar.^(iter-rjk))*xrjk;
Mbar = M/(a^2-1);
qk = q(iter,c,rjk,xrjk,B,D);
chi1b = @(x) chi1(iter,x,D,tk_old,P0,P1,e,state_old);
chi2b = @(x) chi2(iter,x,D,tk_old,P0,P1,e,qk,state_old);
zc = z(iter,c^2,rjk,xrjk);

G = chi1b(abar^2)*xk^2 + 2*(chi1b(abar*a)-chi1b(abar^2))*xk*ek + ...
    (chi1b(a^2) - 2*chi1b(a*abar) + chi1b(abar^2))*ek^2 + Mbar*(chi1b(a^2)-chi1b(1)) -...
    (zc*chi1b(c^2) + B*chi2b(1) - zc*chi2b(c^2));
J = chi1b(abar^2)*xk^2 + ...
    Mbar*(chi1b(a^2)-chi1b(1)) -...
    (zc*chi1b(c^2) + B*chi2b(1) - zc*chi2b(c^2));


if G<0
    tk = 0;
else
    tk = 1;
end

n = size(P0,1);
for j = 1:n
    P1d(j,:) = P1(j,:)*(1-e(j));
    P1e(j,:) = P1(j,:)*e(j);
end


end