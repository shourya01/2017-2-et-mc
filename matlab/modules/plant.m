function [xnew,iter_new] = plant(x,u,a,M,iter_cur)

% Code

rng('shuffle');
v = normrnd(0,M.^2);

xnew = a*x + u + v;
iter_new = iter_cur+1;

end