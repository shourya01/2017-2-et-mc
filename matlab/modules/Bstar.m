function [Bstar, FUBstar] = Bstar(a,abar,c,M)

Mbar = M/(a^2-1);

P1 = log(a^2/abar^2);
P2 = log(a^2 * c^2/abar^2);
P3 = log(1/c^2);
P4 = log( log(1/abar^2)/ (Mbar*log(a^2)) );

B0 = exp(-P4);

%U(B), sst(U(B)), Y(B), Fst(U(B)), dFst(U(B))
U = @(B) exp(P3*P4/P2)*B^(P1/P2);
sU = @(B) log(B)/P2 + P4/P2;
Y = @(B) abar^(2*sU(B))*U(B) + Mbar*a^(2*sU(B));
FU = @(B) Y(B) - Mbar - B;
dFU = @(B) Y(B)*log(a^2)/(P2*B) - 1;

dFUsq = @(B) dFU(B)^2;

if dFU(B0) <= 0
    Bstar = B0;
else
    Bstart = fminsearch(dFUsq, B0);
    Bend = 10*Bstart;
    
    while FU(Bend) > 0
        Bend = Bend + 10*Bstart;
    end
        
    Bstar = fzero(FU, [Bstart, Bend]);
end

FUBstar = FU(Bstar);

if abs( FUBstar ) >= 1e-6
    disp('\ncheck BStar');
end

end