function [zout] = z(iter,b,rjk,xrjk)

% Code

zout = (b.^(iter-rjk))*xrjk.^2;

end