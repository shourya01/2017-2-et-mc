function [qout] = q(iter,c,rjk,xrjk,B,D)

% Code

a1 = ceil(log(xrjk.^2/B)/log(1/(c.^2)));
qout = max([0,a1-(iter-rjk)-D]);

end