function [xc_cur,u] = controller(xc_old,xk,rk,abar,a)

% Code

if rk == 1
    xc_cur = xk;
else
    xc_cur = abar*xc_old;
end

L = abar - a;

u = L*xc_cur;

end
