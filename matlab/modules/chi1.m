function [C] = chi1(iter,b,D,tk_old,P0,P1,e,state_old)

% Code

n = size(P1,1);
% P1d = zeros(n,n);
% P1e = zeros(n,n);
% 
% for j = 1:n
%     P1d(j,:) = P1(j,:)*(1-e(j));
%     P1e(j,:) = P1(j,:)*e(j);
% end

dist = zeros(n,1);
dist(state_old) = 1;
Ptk = tk_old*P1 + (1-tk_old)*P0;
C = Ptk*dist;
C = (P0^(D))*C;
C = inv(eye(n) - b*P1*diag(e))*C;
C = diag(ones(1,n)-e)*C;
C = ones(1,n)*C;
C = (b.^D)*C;

end