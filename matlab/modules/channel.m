function [r_cur,state_new,err_cur] = channel(state_old,t_old,t_cur,P0,P1,e,n)

% Code

if t_old == 1
    P = P1;
else
    P = P0;
end

dist_cur = P(state_old,:);
rng('shuffle');
r = rand;
% r
% cumsum(dist_cur)-r
state_new = sum(cumsum(dist_cur)-r>0);

if state_new > n
    error('New state exceeds maximum states');
end

if t_cur == 0
    r_cur = 0;
    return;
end

err = e(state_new);
err_cur = err;

rng('shuffle');
r = rand;
if r < err
    r_cur = 0;
else
    r_cur = 1;
end

end
    

