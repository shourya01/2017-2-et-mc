% clear all
% close all

addpath functions
addpath modules
addpath scripts

% Initiate system variables

init_var
D = Dpreset;

% Checks

if (a^2)*max(e)>=1
    error("Bad a^2*emax, value = " + (a^2)*max(e));
else
    disp("Good a^2emax confirmed, value = "+(a^2)*max(e));
end

[Bs,FBs] = Bstar(a,abar,c,M);

if B>Bs
    disp("Good B, B = "+B+", Bstar = "+Bs);
else
    error(['Bad B, B = ',num2str(B),', Bstar = ',num2str(Bs)]);
end

Cfunc = @(b,s) chiconf(b,D,P0,P1,e,s);
trig = 0;

for j = 1:n
    val = (Cfunc(abar^2,j)-Cfunc(c^2,j))*(B/c^(2*D)) + ...
        (Cfunc(a^2,j)-Cfunc(1,j))*(M/(a^2-1));
    if val > 0
        trig = 1;
    end
end

if trig == 1
    error('Bad event triggered criteria!');
else
    disp("Good event triggered criteria confirmed!");
end

disp("Necessary and sufficient conditions verified, now starting iterations.");
disp("Change value of sysiter around line 50 in main.m to change no. of emperical iters.");

% Start logging

init_logging

% Log errors
sig = struct('callout',1,'dump',1);

% System iteration
sysiter = 500;
DATALG = [];

% Timestep iteration

for syscount = 1:sysiter
    init_var
    init_logging
    for i = 1:max_iter
        % modifyvar
        %
        %
        %
        % init_var % This will push the modified variables into memory
        
        [xnew,filler1] = plant(data_log.xk(end),data_log.uk(end),a,M,i);
        data_log.xk = [data_log.xk xnew];
        
        [eknew,tknew,Gnew,Jnew] = sensor(i,data_log.rjk(end),data_log.xrjk(end),B,D,data_log.tk(end),...
            data_log.cstate(end),data_log.xk(end),abar,a,c,M,P0,P1,e,data_log.xc(end));
        data_log.ek = [data_log.ek eknew];
        data_log.tk = [data_log.tk tknew];
        data_log.G = [data_log.G Gnew];
        data_log.G(1) = data_log.G(2);
        data_log.J = [data_log.J Gnew];
        data_log.J(1) = data_log.J(2);
        
        [rknew,statenew] = channel(data_log.cstate(end),data_log.tk(end-1),data_log.tk(end),...
            P0,P1,e,n);
        data_log.rk = [data_log.rk rknew];
        data_log.cstate = [data_log.cstate statenew];
        if rknew == 1
            data_log.rjk = [data_log.rjk i];
            data_log.xrjk = [data_log.xrjk xnew];
        else
            data_log.rjk = [data_log.rjk data_log.rjk(end)];
            data_log.xrjk = [data_log.xrjk data_log.xrjk(end)];
        end
        
        [xcnew,unew] = controller(data_log.xc(end),data_log.xk(end),data_log.rk(end),...
            abar,a);
        data_log.uk = [data_log.uk unew];
        data_log.xc = [data_log.xc xcnew];
    end
    disp("Completed iteration "+syscount);
    DATALG = [DATALG data_log];
    lastdatalg = data_log;
    clearvars -except DATALG sysiter lastdatalg
end
init_var
init_logging
data_log = lastdatalg;

% Averaging ops
Xavg = zeros(1,max_iter+1);
Gavg = zeros(1,max_iter+1);
TFavg = zeros(1,max_iter);
CSavg = zeros(1,n);
for w = 1:sysiter
    Dcur = DATALG(w);
    Xavg = Xavg + Dcur.xk;
    Gavg = Gavg + Dcur.G;
    TFavg = TFavg + (cumsum(Dcur.rk(2:end))./(1:max_iter));
    CScur = [];
    for w = 1:n
        CScur = [CScur sum(Dcur.cstate==w)/(max_iter+1)];
    end
    CSavg = CSavg + CScur;
end
Xavg = Xavg/sysiter;
Gavg = Gavg/sysiter;
TFavg = TFavg/sysiter;
CSavg = CSavg/sysiter;

% Theoretical TF
% This gon get long and dirty, don't have time to make a separate script
% for this

Dcur = D;
Bscript = 0;

while(true)
    Cfunc = @(b,s) chiconf(b,Dcur + Bscript,P0,P1,e,s);
    trig = 0;
    for j = 1:n
        val = (Cfunc(abar^2,j)-Cfunc(c^2,j))*(B/c^(2*D)) + ...
            (Cfunc(a^2,j)-Cfunc(1,j))*(M/(a^2-1));
        if val > 0
            trig = 1;
        end
    end
    if trig == 1
        Bscript = max([Bscript - 1,0]);
        break;
    else
        Bscript = Bscript+1;
    end
end

betavec = [];
for y = 1:n
    evec = zeros(n,1);
    evec(y) = 1;
    L = ones(1,n)*diag(ones(1,n)-e)*pinv(eye(n)-P1*diag(e))*evec;
    betavec = [betavec L];
end

betaval = max(betavec);
TFtheo = betaval/(betaval+Bscript);

% Save variables

save(['resultsD',num2str(D),'.mat'],'Xavg','Gavg','TFavg','CSavg','TFtheo');

% Plot

% Envelope

if plot_enable==1
    
    cenv = @(x) (data_log.xk(1)^2)*(c.^(2*x));
    Benv = @(x) B + 0.*x;
    timestamp = 0:max_iter;
    env = log(max([cenv(timestamp);Benv(timestamp)]));
    figure;
    box on;
    hold on;
    plot(timestamp,env,'--k','linewidth',1);
    plot(timestamp,log(Xavg.^2),'r','linewidth',1);
    xlabel('Timesteps')
    ylabel('Squared state and envelope (log)');
    
    figure;
    box on;
    hold on;
    plot(timestamp,Gavg,'m','linewidth',1);
    plot(timestamp,zeros(1,numel(timestamp)),'--k');
    xlabel('Timesteps');
    ylabel('G^D_k');
    
    figure;
    box on;
    hold on;
    plot(timestamp(2:end),TFavg,'m','linewidth',1);
    plot(timestamp(2:end),TFtheo*ones(1,numel(timestamp(2:end))),'--k');
    xlabel('Timesteps');
    ylabel('Emperical Transmission Fraction (solid), Theoretical (dotted)');
    
end
