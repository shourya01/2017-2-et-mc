List of things to do in a rough priority order:

1. Change MDP formulation to MC (P0 = P1 = P)

2. Sim results - complete with all details including various plots we
discussed earlier

3. Complete the proofs of Theorem 4(a) and 4(c)

4. Add a full proof for Theorem 5 (we can hide it later if required)

5. List of notation before problem formulation
