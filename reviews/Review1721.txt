Reviewer 3 of ICC 2019 submission 171

Comments to the author
======================

This paper deals with the stabilization of a linear systems
with process noise under packet drops between the sensor
and the controller. Objective is to ensure exponential
convergence of the second moment of the plant state to a
given bound in finite time. 

This paper is indeed an extension of the already published
paper [21]. However, analysis in the context of Markov
packet drop is non-trivial and interesting. 

This is a well written paper. Presentation is clear and
proofs are presented rigorously.  Technical contribution is
also good.

